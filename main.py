import csv

import matplotlib.pyplot as plt


def main():
    f_coordinates = []
    omega_coordinates = []
    pi_coordinates = []
    dicts_by_letter = {'Ф (Ф)': f_coordinates, 'Омега (Ω)': omega_coordinates, 'Пи (π)': pi_coordinates}
    with open('./assets/lab1_nodes&ends.csv') as table:
        reader = csv.DictReader(table, fieldnames=['1', '2', '3', '4'])
        actual_dict = {}
        for i, row in enumerate(reader):
            new_dict = dicts_by_letter.get(row.get('1'))
            if new_dict is not None:
                actual_dict = new_dict
            actual_dict.append({'x': int(row['2']), 'y': int(row['3'])})

    f_x = [obj['x'] for obj in f_coordinates]
    f_y = [obj['y'] for obj in f_coordinates]
    omega_x = [obj['x'] for obj in omega_coordinates]
    omega_y = [obj['y'] for obj in omega_coordinates]
    pi_x = [obj['x'] for obj in pi_coordinates]
    pi_y = [obj['y'] for obj in pi_coordinates]

    plt.scatter(f_x, f_y,
                c='blue', label='Ф', marker='>')
    plt.scatter(omega_x, omega_y,
                c='green', label='Ω', marker='^')
    plt.scatter(pi_x, pi_y,
                c='red', label='π', marker='<')

    plt.xlabel('Кол-во узловых точек')
    plt.ylabel('Кол-во концевых точек')
    plt.title('Визуализация объектов в пространстве признаков')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
